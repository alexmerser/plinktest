from logging import getLogger

from drfpasswordless.utils import create_callback_token_for_user

logger = getLogger(__name__)


def send_to_console(user, token, msgpayload) -> bool:

    logger.debug("USER - %s\n TOKEN - %s\n PAYLOAD - %s\n", user, token, msgpayload)
    return True


class TokenService(object):
    @staticmethod
    def send_token(user, alias_type, **message_payload):
        token = create_callback_token_for_user(user, alias_type)
        send_action = send_to_console
        success = send_action(user, token, **message_payload)
        return success
