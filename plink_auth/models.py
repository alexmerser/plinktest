from logging import getLogger

from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import BaseUserManager
from django.db import models

from phonenumber_field.modelfields import PhoneNumberField

logger = getLogger(__name__)


class User(AbstractBaseUser):
    mobile = PhoneNumberField(unique=True)
    mobile_verified = models.BooleanField(default=False)

    objects = BaseUserManager()

    USERNAME_FIELD = 'mobile'

    class Meta:
        app_label = 'plink_auth'
