from django.apps import AppConfig


class PlinkAuthConfig(AppConfig):
    name = 'plink_auth'
