from logging import getLogger

from drfpasswordless.serializers import MobileAuthSerializer
from drfpasswordless.settings import api_settings
from rest_framework import parsers, renderers, status
from rest_framework.response import Response
from rest_framework.views import APIView

from plink_auth.services import TokenService as DefaultTockenService

logger = getLogger(__name__)


class AbstractBaseObtainCallbackToken(APIView):
    """
    This returns a 6-digit callback token we can trade for a user's Auth Token.
    """
    throttle_classes: tuple = ()
    permission_classes: tuple = ()
    parser_classes: tuple = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    renderer_classes: tuple = (renderers.JSONRenderer,)

    success_response: str = "A login token has been sent to you."
    failure_response: str = "Unable to send you a login code. Try again later."

    message_payload: dict = {}

    token_service: DefaultTockenService = DefaultTockenService

    @property
    def serializer_class(self):
        raise NotImplementedError

    @property
    def alias_type(self):
        raise NotImplementedError

    def post(self, request, *args, **kwargs):
        if self.alias_type.upper() not in api_settings.PASSWORDLESS_AUTH_TYPES:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = self.serializer_class(data=request.data, context={'request': request})
        if serializer.is_valid(raise_exception=True):
            user = serializer.validated_data['user']
            success = self.token_service.send_token(user, self.alias_type, **self.message_payload)
            if success:
                status_code = status.HTTP_200_OK
                response_detail = self.success_response
            else:
                status_code = status.HTTP_400_BAD_REQUEST
                response_detail = self.failure_response
            return Response({'detail': response_detail}, status=status_code)
        else:
            return Response(serializer.error_messages, status=status.HTTP_400_BAD_REQUEST)


class ObtainMobileCallbackToken(AbstractBaseObtainCallbackToken):
    serializer_class = MobileAuthSerializer
    success_response = "We texted you a login code."
    failure_response = "Unable to send you a login code. Try again later."

    alias_type = 'mobile'

    mobile_message = api_settings.PASSWORDLESS_MOBILE_MESSAGE
    message_payload = {'mobile_message': mobile_message}
