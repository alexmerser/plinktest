"""Auth urls router
"""
from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from drfpasswordless.views import (ObtainMobileCallbackToken,
                                   ObtainAuthTokenFromCallbackToken,
                                   VerifyAliasFromCallbackToken,
                                   ObtainMobileVerificationCallbackToken)


auth_urlpatterns: list = [
    url(r'^callback/auth/', ObtainAuthTokenFromCallbackToken.as_view(), name='auth_callback'),
    url(r'^auth/mobile/', ObtainMobileCallbackToken.as_view(), name='auth_mobile'),
    url(r'^callback/verify/', VerifyAliasFromCallbackToken.as_view(), name='verify_callback'),
    url(r'^verify/mobile/', ObtainMobileVerificationCallbackToken.as_view(), name='verify_mobile')]

AUTH_ROUTER = DefaultRouter()

urlpatterns = AUTH_ROUTER.urls
urlpatterns.extend(auth_urlpatterns)
