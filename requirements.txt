Babel==2.5.3
Django==2.0.4
django-messages==0.5.3
django-phonenumber-field==2.0.0
djangorestframework==3.8.2
drfpasswordless==1.1.5
phonenumberslite==8.9.3
pytz==2018.4
