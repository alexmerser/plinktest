from django.apps import AppConfig


class MessagesConfig(AppConfig):
    name = 'plink_messages'
