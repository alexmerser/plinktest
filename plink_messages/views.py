from django.db.models import QuerySet, Model
from rest_framework import permissions, viewsets

from plink_messages.models import Message
from plink_messages.serializers import MessageSerializer, CreateMessageSerializer
from django.contrib.auth import get_user_model


class MessageViewSet(viewsets.ModelViewSet):

    serializer_class = MessageSerializer
    permission_classes = (permissions.IsAuthenticated,
                          )

    USER_MODEL: Model = get_user_model()

    def get_queryset(self) -> QuerySet:
        return Message.objects.all()

    def perform_create(self, serializer) -> Message:
        serializer = CreateMessageSerializer(serializer.data)
        serializer.save()
        return serializer.data
