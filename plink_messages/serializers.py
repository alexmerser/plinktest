from django.contrib.auth import get_user_model
from rest_framework import serializers
from plink_messages.models import Message

USER_MODEL = get_user_model()


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = USER_MODEL
        fields = ('id', 'mobile', 'mobile_verified')


class MessageSerializer(serializers.ModelSerializer):
    sender = UserSerializer(many=False, read_only=True)
    receiver = UserSerializer()
    read = serializers.BooleanField(source='is_read', default=False)

    class Meta:
        model = Message
        fields = ("sender", "receiver", "text", "created", 'read')


class CreateMessageSerializer(serializers.HyperlinkedModelSerializer):
    sender = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Message
        fields = ('text', 'receiver', 'sender')

    def create(self, validated_data):
        message = Message(
            sender=validated_data['sender'],
            receiver=validated_data['receiver'],
            text=validated_data['text'])
        message.save()
        return message
