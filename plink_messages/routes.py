from rest_framework.routers import DefaultRouter

from plink_messages.views import MessageViewSet

message_api = DefaultRouter(trailing_slash=False)

message_api.register(r'^messages/', MessageViewSet, base_name='message')

urlpatterns = message_api.urls
