from django.contrib.auth.models import User
from django.db import models
from django.contrib.auth import get_user_model


USER_MODEL = get_user_model()


class Message(models.Model):
    sender = models.ForeignKey(USER_MODEL, on_delete=models.CASCADE, related_name='outbox')
    receiver = models.ForeignKey(USER_MODEL, on_delete=models.CASCADE, related_name='inbox', default=None, null=True)
    text = models.CharField(max_length=120)
    created = models.DateTimeField(auto_now_add=True)
    read = models.DateTimeField(default=None, null=True, blank=True)

    @property
    def is_read(self) -> bool:
        return bool(self.read)

    def __str__(self):
        return self.text

    class Meta:
        ordering = ('created',)

